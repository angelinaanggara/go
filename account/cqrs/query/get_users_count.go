package query

import (
	"context"

	"github.com/go-kit/kit/log"

	domain "xxx/account/domain"
)

// UsersCountQueryHandler is to get users count
type UsersCountQueryHandler struct {
	logger     log.Logger
	repository domain.Repository
}

/**
 * NewUsersCountQueryHandler initialization
 *
 * @param  {Repository} repo 								The repository
 * @return {NewUsersCountQueryHandler} 			The query handler
 */
func NewUsersCountQueryHandler(repo domain.Repository) UsersCountQueryHandler {
	return UsersCountQueryHandler{
		repository: repo,
	}
}

/**
 * Get user by id
 *
 * @param  {Context} ctx           					The context
 * @return {(int64, error)}       					Success: The users count; Failed: The error message
 */
func (handler UsersCountQueryHandler) Handle(ctx context.Context) (int64, error) {
	usersCount, err := handler.repository.GetUsersCount(ctx)

	return usersCount, err
}
