package crqs

import (
	command "xxx/account/cqrs/command"
	query "xxx/account/cqrs/query"
	domain "xxx/account/domain"
)

// CQRSFacade consists Commands & Queries
type CQRSFacade struct {
	Commands Commands
	Queries  Queries
}

// Commands is to handle write data
type Commands struct {
	CreateUser command.CreateUserCommandHandler
	UpdateUser command.UpdateUserCommandHandler
	DeleteUser command.DeleteUserCommandHandler
}

// Queries is to handle read data
type Queries struct {
	GetUsers      query.UsersQueryHandler
	GetUsersCount query.UsersCountQueryHandler
	GetUser       query.UserQueryHandler
}

/**
 * NewCQRSFacade initialization
 *
 * @param  {Repository} repo 								The repository
 * @return {CreateUserCommandHandler} 			The command handler
 */
func NewCQRSFacade(repo domain.Repository) CQRSFacade {
	return CQRSFacade{
		Commands: Commands{
			CreateUser: command.NewCreateUserCommandHandler(repo),
			UpdateUser: command.NewUpdateUserCommandHandler(repo),
			DeleteUser: command.NewDeleteUserCommandHandler(repo),
		},
		Queries: Queries{
			GetUsers:      query.NewUsersQueryHandler(repo),
			GetUsersCount: query.NewUsersCountQueryHandler(repo),
			GetUser:       query.NewUserQueryHandler(repo),
		},
	}
}
