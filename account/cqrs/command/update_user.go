package command

import (
	"context"

	domain "xxx/account/domain"

	"github.com/google/uuid"
)

// UpdateUserCommand is to edit user
type UpdateUserCommand struct {
	TransactionID uuid.UUID
	ID            string
	User          domain.User
}

// UpdateUserCommandHandler is to handle UpdateUserCommand
type UpdateUserCommandHandler struct {
	repository domain.Repository
}

/**
 * NewUpdateUserCommandHandler initialization
 *
 * @param  {Repository} repo 								The repository
 * @return {UpdateUserCommandHandler} 			The command handler
 */
func NewUpdateUserCommandHandler(repo domain.Repository) UpdateUserCommandHandler {
	return UpdateUserCommandHandler{
		repository: repo,
	}
}

/**
 * Update a user
 *
 * @param  {Context} ctx           					The context
 * @param  {UpdateUserCommand} command     	The create user command
 * @return {(User, error)}       						Success: The updated user; Failed: The error message
 */
func (handler UpdateUserCommandHandler) Handle(ctx context.Context, command UpdateUserCommand) (domain.User, error) {
	existingUser, err := handler.repository.GetUser(ctx, command.ID)

	if err != nil {
		return existingUser, err
	}

	command.User.ID = existingUser.ID
	updatedUser, err := handler.repository.UpdateUser(ctx, command.User)

	if err != nil {
		return updatedUser, err
	}

	return updatedUser, nil
}
