package account

import (
	"context"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/google/uuid"

	automapper "xxx/+core/utils/automapper"
	natsutil "xxx/+core/utils/nats"
	accountCQRS "xxx/account/cqrs"
	accountCommand "xxx/account/cqrs/command"
	accountQuery "xxx/account/cqrs/query"
	accountDomain "xxx/account/domain"
)

// Service to handle user stuff
type Service interface {
	/**
	 * Get users
	 *
	 * @param  {context.Context} ctx 							The context
	 * @param  {GetUsersRequest} request     			The get users request
	 * @return {(GetUsersResponse, error)} 				Success -> paginated users list; Failed -> error
	 */
	GetUsers(ctx context.Context, request GetUsersRequest) (GetUsersResponse, error)

	/**
	 * Get users count
	 *
	 * @param  {context.Context} ctx 							The context
	 * @return {(GetUsersResponse, error)} 				Success -> users count; Failed -> error
	 */
	GetUsersCount(ctx context.Context) (GetUsersCountResponse, error)

	/**
	 * Create a user
	 *
	 * @param  {Context} ctx           						The context
	 * @param  {CreateUserRequest} request     		The create user request
	 * @return {(CreateUserResponse, error)}    	Success: The user; Failed: The error message
	 */
	CreateUser(ctx context.Context, request CreateUserRequest) (CreateUserResponse, error)

	/**
	 * Get user by id
	 *
	 * @param  {Context} ctx           						The context
	 * @param  {string} id             						The user id
	 * @return {(GetUserResponse, error)}       	Success: The user; Failed: The error message
	 */
	GetUser(ctx context.Context, id string) (GetUserResponse, error)

	/**
	 * Update user
	 *
	 * @param  {Context} ctx           						The context
	 * @param  {string} id             						The user id
	 * @return {(PutUserResponse, error)}       	Success: The user; Failed: The error message
	 */
	UpdateUser(ctx context.Context, request PutUserRequest) (PutUserResponse, error)

	/**
	 * Delete user by id
	 *
	 * @param  {Context} ctx           						The context
	 * @param  {string} id             						The user id
	 * @return {(error)}       										Success: nil; Failed: The error message
	 */
	DeleteUser(ctx context.Context, id string) error
}

////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
////////////////////////////////////////////////////////////////////////////////////////////

type service struct {
	logger             log.Logger
	streamingComponent *natsutil.StreamingComponent
	commands           accountCQRS.Commands
	queries            accountCQRS.Queries
}

/**
 * NewService initialization
 *
 * @param  {Logger} logger 									The logger
 * @param  {CQRSFacade} cqrsFacade 					The cqrs facade
 * @param  {StreamingComponent} natsComp 		The NATS Streaming component
 * @return {Service} 												The service
 */
func NewService(logger log.Logger, cqrsFacade accountCQRS.CQRSFacade, streamingComponent *natsutil.StreamingComponent) Service {
	return &service{
		logger:             logger,
		streamingComponent: streamingComponent,
		commands:           cqrsFacade.Commands,
		queries:            cqrsFacade.Queries,
	}
}

/**
 * Get users
 *
 * @param  {context.Context} ctx 							The context
 * @return {(GetUsersResponse, error)} 				Success -> paginated users list; Failed -> error
 */
func (srv service) GetUsers(ctx context.Context, request GetUsersRequest) (GetUsersResponse, error) {
	logger := log.With(srv.logger, "method", "GetUsers")
	query := accountQuery.UsersQuery{}

	automapper.Map(&request, &query)

	response := GetUsersResponse{}
	paginator, err := srv.queries.GetUsers.Handle(ctx, query)

	if err != nil {
		level.Error(logger).Log("get-users-failed", err)

		return response, err
	}

	// This is a trick to reset counter...not a real case :)
	srv.streamingComponent.PublishMessage(accountDomain.ActionResetUserCounter, natsutil.Message{
		ID:      uuid.New(),
		Payload: paginator.TotalRecord,
	})

	automapper.Map(&paginator, &response)

	return response, nil
}

/**
 * Get users count
 *
 * @param  {context.Context} ctx 							The context
 * @return {(GetUsersResponse, error)} 				Success -> users count; Failed -> error
 */
func (srv service) GetUsersCount(ctx context.Context) (GetUsersCountResponse, error) {
	logger := log.With(srv.logger, "method", "GetUsersCount")
	usersCount, err := srv.queries.GetUsersCount.Handle(ctx)
	res := GetUsersCountResponse{TotalRecord: usersCount}

	if err != nil {
		level.Error(logger).Log("get-user-failed", err)

		return res, err
	}

	return res, nil
}

/**
 * Create a user
 *
 * @param  {Context} ctx           					The context
 * @param  {CreateUserRequest} request     	The create user request
 * @return {(CreateUserResponse, error)}  	Success: The user; Failed: The error message
 */
func (srv service) CreateUser(ctx context.Context, request CreateUserRequest) (CreateUserResponse, error) {
	// TODO: validations
	logger := log.With(srv.logger, "method", "CreateUser")
	command := accountCommand.CreateUserCommand{
		TransactionID: uuid.New(),
		ID:            uuid.New(),
		Name:          request.Name,
		Email:         request.Email,
		Age:           request.Age,
	}
	response := CreateUserResponse{}
	user, err := srv.commands.CreateUser.Handle(ctx, command)

	if err != nil {
		level.Error(logger).Log("create-user-failed", err)

		return response, err
	}

	// Publish message
	srv.streamingComponent.PublishMessage(accountDomain.UserCreatedEvent, natsutil.Message{
		ID:      command.TransactionID,
		Payload: user,
	})

	automapper.Map(&user, &response)

	return response, err
}

/**
 * Get user by id
 *
 * @param  {context.Context} ctx 								The context
 * @param  {string} id		 											The user id
 * @return {(GetUserResponse, error)} 					Success -> user; Failed -> error
 */
func (srv service) GetUser(ctx context.Context, id string) (GetUserResponse, error) {
	logger := log.With(srv.logger, "method", "GetUser")
	user, err := srv.queries.GetUser.Handle(ctx, id)
	res := GetUserResponse{}

	if err != nil {
		level.Error(logger).Log("get-user-failed", err)

		return res, err
	}

	automapper.Map(&user, &res)

	return res, nil
}

/**
 * Update user
 *
 * @param  {Context} ctx           						The context
 * @param  {string} id             						The user id
 * @return {(PutUserResponse, error)}       	Success: The user; Failed: The error message
 */
func (srv service) UpdateUser(ctx context.Context, request PutUserRequest) (PutUserResponse, error) {
	logger := log.With(srv.logger, "method", "UpdateUser")
	command := accountCommand.UpdateUserCommand{
		TransactionID: uuid.New(),
		ID:            request.ID,
		User:          accountDomain.User{},
	}

	automapper.Map(&request, &command.User)

	user, err := srv.commands.UpdateUser.Handle(ctx, command)
	res := PutUserResponse{}

	if err != nil {
		level.Error(logger).Log("update-user-failed", err)

		return res, err
	}

	// Publish message
	srv.streamingComponent.PublishMessage(accountDomain.UserUpdatedEvent, natsutil.Message{
		ID:      command.TransactionID,
		Payload: user,
	})

	automapper.Map(&user, &res)

	return res, nil
}

/**
 * Update user
 *
 * @param  {Context} ctx           						The context
 * @param  {string} id             						The user id
 * @return {(error)}       										Success: nil; Failed: The error message
 */
func (srv service) DeleteUser(ctx context.Context, id string) error {
	logger := log.With(srv.logger, "method", "DeleteUser")

	userID, err := srv.commands.DeleteUser.Handle(ctx, id)

	if err != nil {
		level.Error(logger).Log("delete-user-failed", err)

		return err
	}

	// Publish message
	srv.streamingComponent.PublishMessage(accountDomain.UserDeletedEvent, natsutil.Message{
		ID:      uuid.New(),
		Payload: userID,
	})

	return nil
}
