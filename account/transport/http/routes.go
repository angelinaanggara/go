package http

import (
	"context"
	"net/http"

	kitTransportHttp "github.com/go-kit/kit/transport/http"

	"github.com/gorilla/mux"

	middlewares "xxx/+core/middleware"
	serializer "xxx/+core/serializer/json"

	transport "xxx/account/transport"
)

/**
 * New HTTPServer
 *
 * @param  {context.Context} ctx 					The context
 * @param  {Endpoints} endpoints 					The account endpoints
 * @return {http.Handler} 								The http handler
 */
func InitRoutes(ctx context.Context, endpoints transport.Endpoints) http.Handler {
	router := mux.NewRouter()
	errorEncoder := kitTransportHttp.ServerErrorEncoder(serializer.EncodeErrorResponse)
	options := []kitTransportHttp.ServerOption{errorEncoder}

	// Apply middlewares
	router.Use(middlewares.ContentTypeJSONMiddleware)

	// Routes
	router.Methods(http.MethodGet).Path("/users/report").Handler(kitTransportHttp.NewServer(
		endpoints.Report,
		decodeReportRequest,
		serializer.EncodeResponse,
		options...,
	))

	router.Methods(http.MethodGet).Path("/users").Handler(kitTransportHttp.NewServer(
		endpoints.GetUsers,
		decodeGetUsersRequest,
		serializer.EncodeResponse,
		options...,
	))

	router.Methods(http.MethodPost).Path("/users").Handler(kitTransportHttp.NewServer(
		endpoints.CreateUser,
		decodeCreateUserRequest,
		serializer.EncodeResponseWithStatus(http.StatusCreated),
		options...,
	))

	router.Methods(http.MethodGet).Path("/users/{id}").Handler(kitTransportHttp.NewServer(
		endpoints.GetUser,
		decodeGetUserRequest,
		serializer.EncodeResponse,
		options...,
	))

	router.Methods(http.MethodPut).Path("/users/{id}").Handler(kitTransportHttp.NewServer(
		endpoints.PutUser,
		decodePutUserRequest,
		serializer.EncodeResponse,
		options...,
	))

	router.Methods(http.MethodDelete).Path("/users/{id}").Handler(kitTransportHttp.NewServer(
		endpoints.DeleteUser,
		decodeDeleteUserRequest,
		serializer.EncodeResponseWithStatus(http.StatusNoContent),
		options...,
	))

	return router
}
