package account

import (
	"gorm.io/gorm"
)

// UserCounter model
type UserCounter struct {
	gorm.Model
	All      int64 `json:"all"`
	Active   int64 `json:"active"`
	Inactive int64 `json:"inactive"`
}
