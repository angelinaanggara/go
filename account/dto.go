package account

// GetUsersRequest type
type GetUsersRequest struct {
	Limit  int    `json:"limit,omitempty"`
	Page   int    `json:"page,omitempty"`
	SortBy string `json:"sortBy,omitempty"`
	Search string `json:"search,omitempty"`
}

// GetUsersResponse type
type GetUsersResponse struct {
	TotalRecord int64             `json:"totalRecord"`
	TotalPage   int               `json:"totalPage"`
	Offset      int               `json:"offset"`
	Limit       int               `json:"limit"`
	Page        int               `json:"page"`
	PrevPage    int               `json:"prevPage"`
	NextPage    int               `json:"nextPage"`
	Records     []GetUserResponse `json:"records"`
}

// GetUsersCountResponse type
type GetUsersCountResponse struct {
	TotalRecord int64 `json:"totalRecord"`
}

// CreateUserRequest type
type CreateUserRequest struct {
	Email string `json:"email"`
	Name  string `json:"name"`
	Age   int    `json:"age"`
}

// CreateUserResponse type
type CreateUserResponse struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Age    int    `json:"age"`
	Active bool   `json:"active"`
}

// GetUserRequest type
type GetUserRequest struct {
	ID string `json:"id"`
}

// GetUserResponse type
type GetUserResponse struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Age    int    `json:"age"`
	Active bool   `json:"active"`
}

// PutUserRequest type
type PutUserRequest struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Age    int    `json:"age"`
	Active bool   `json:"active"`
}

// PutUserResponse type
type PutUserResponse struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Age    int    `json:"age"`
	Active bool   `json:"active"`
}

// DeleteUserRequest type
type DeleteUserRequest struct {
	ID string `json:"id"`
}

// DeleteUserResponse type
type DeleteUserResponse struct {
}
