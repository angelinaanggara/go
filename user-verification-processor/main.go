package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/google/uuid"
	stan "github.com/nats-io/stan.go"

	envutil "xxx/+core/utils/env"
	natsutil "xxx/+core/utils/nats"
	account "xxx/account/domain"
)

const (
	ClientID   = "user-verification-processor"
	DurableID  = "user-verification-processor-durable"
	QueueGroup = "user-queue-group"
)

func main() {
	// Register new NATS component within the system.
	streamingComp := natsutil.NewStreamingComponent(ClientID)

	// Connect to NATS Streaming server
	err := streamingComp.ConnectToNATSStreaming(
		envutil.GetEnv("NATS_CLUSTER_ID", "test-cluster"),
		stan.NatsURL(stan.DefaultNatsURL),
	)

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("%s started", ClientID)

	// Init error channel
	errorChannel := make(chan error)

	// Init signal notifications
	go initSignalNotifications(errorChannel)

	// Subscribe on action verify user
	go subscribeActionVerifyUser(streamingComp)

	// Subscribe on action rollback
	go subscribeActionRollbackUserCreation(streamingComp)

	// Cleanup
	<-errorChannel

	streamingComp.Shutdown()

	log.Printf("%s stopped", ClientID)
}

// Init signal notifications
func initSignalNotifications(errorChannel chan error) {
	signalChannel := make(chan os.Signal, 1)

	signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGTERM)
	errorChannel <- fmt.Errorf("%s", <-signalChannel)
}

// Handle ActionVerifyUser
func subscribeActionVerifyUser(streamingComp *natsutil.StreamingComponent) {
	// Get the NATS Streaming Connection
	sc := streamingComp.NATS()
	sc.QueueSubscribe(account.ActionVerifyUser, QueueGroup, func(msg *stan.Msg) {
		message := natsutil.Message{}
		err := json.Unmarshal(msg.Data, &message)

		if err != nil || message.Payload == nil || message.ID == uuid.Nil {
			log.Printf("%s received bad message: %+v\n", streamingComp.Name(), msg)
			return
		}

		// Handle the message
		log.Printf("%s subscribed message: %+v\n", ClientID, message)

		// Sleep 10 seconds
		time.Sleep(10 * time.Second)

		// Publish new event
		streamingComp.PublishMessage(account.UserVerifiedEvent, natsutil.Message{
			ID:      message.ID,
			Payload: message.Payload,
		})
	}, stan.DurableName(DurableID))
}

// Handle ActionRollbackUserCreation
func subscribeActionRollbackUserCreation(streamingComp *natsutil.StreamingComponent) {
	// Get the NATS Streaming Connection
	sc := streamingComp.NATS()
	sc.QueueSubscribe(account.ActionRollbackUserCreation, QueueGroup, func(msg *stan.Msg) {
		message := natsutil.Message{}
		err := json.Unmarshal(msg.Data, &message)

		if err != nil || message.Payload == nil || message.ID == uuid.Nil {
			log.Printf("%s received bad message: %+v\n", streamingComp.Name(), msg)
			return
		}

		// Handle the message
		log.Printf("%s rolling back transaction with ID :%s", streamingComp.Name(), message.ID)
	}, stan.DurableName(DurableID))
}
