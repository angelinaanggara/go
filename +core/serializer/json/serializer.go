package serializer

import (
	"context"
	"encoding/json"
	"net/http"

	"xxx/+core/errors"
)

type errorer interface {
	error() error
}

/**
 * Response encoder
 *
 * @param  {context.Context} ctx 									The context
 * @param  {http.ResponseWriter} responseWriter 	The response writer
 * @param  {interface{}} response 								The response
 * @return {error}																Success -> nil; Failed -> error
 */
func EncodeResponse(ctx context.Context, responseWriter http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(responseWriter).Encode(response)
}

/**
 * Encode response with http status
 *
 * @param  {int} status 													The status code
 * @return {func EncodeResponse}
 */
func EncodeResponseWithStatus(status int) func(ctx context.Context, responseWriter http.ResponseWriter, response interface{}) error {
	return func(ctx context.Context, responseWriter http.ResponseWriter, response interface{}) error {
		responseWriter.WriteHeader(status)

		return EncodeResponse(ctx, responseWriter, response)
	}
}

/**
 * StatusNoContent encoder
 *
 * @param  {context.Context} ctx 									The context
 * @param  {http.ResponseWriter} responseWriter 	The response writer
 * @param  {interface{}} response 								The response
 * @return {error}																Success -> nil; Failed -> error
 */
func NoContentResponse(ctx context.Context, responseWriter http.ResponseWriter, response interface{}) error {
	responseWriter.WriteHeader(http.StatusNoContent)

	return nil
}

/**
 * StatusCreated encoder
 *
 * @param  {context.Context} ctx 									The context
 * @param  {http.ResponseWriter} responseWriter 	The response writer
 * @param  {interface{}} response 								The response
 * @return {error}																Success -> nil; Failed -> error
 */
func CreatedResponse(ctx context.Context, responseWriter http.ResponseWriter, response interface{}) error {
	responseWriter.WriteHeader(http.StatusCreated)

	return nil
}

/**
 * Error response encoder
 *
 * @param  {context.Context} ctx 									The context
 * @param  {error} err 														The error
 * @param  {http.ResponseWriter} responseWriter 	The response writer
 * @return {void}
 */
func EncodeErrorResponse(ctx context.Context, err error, responseWriter http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}

	responseWriter.WriteHeader(codeFrom(err))
	json.NewEncoder(responseWriter).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

/**
 * Map HTTP code from error
 *
 * @param  {error} err 					The error
 * @return {int} 								The HTTP code
 */
func codeFrom(err error) int {
	switch err {
	case errors.ErrDataNotFound:
		return http.StatusNotFound
	default:
		return http.StatusInternalServerError
	}
}
