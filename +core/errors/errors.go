package errors

import (
	"errors"
)

var (
	// ErrBadRouting error
	ErrBadRouting = errors.New("Inconsistent mapping between route and handler (programmer error)")

	// ErrDataNotFound error
	ErrDataNotFound = errors.New("Data not found")

	// ErrBadRequest error
	ErrBadRequest = errors.New("Bad request")

	// ErrCmdRepository error
	ErrCmdRepository = errors.New("Unable to command repository")

	// ErrQueryRepository error
	ErrQueryRepository = errors.New("Unable to query repository")
)
