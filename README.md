# GO
## Version
0.0.1

## Summary
This is an example to demonstrate a simple CRUD RESTful service using CQRS and Event Sourcing.\
There're some points I want to add in/take into consideration:
  - Swagger docs
  - Http exception handling
  - Validate inputs
  - Create a EventStore service to persist all immutable events happened in the system to DB
  - Change Data Capture ([CDC](https://www.cockroachlabs.com/docs/v20.2/stream-data-out-of-cockroachdb-using-changefeeds.html)) maybe?
  - Docker compose

### User Creation Flow
  -> Account service dispatch a command to store user to DB with `Active=false`\
  -> `saga/create-user` acts as a saga orchestrator to handle `User Creation flow`, handle business and rollback if something went wrong (currently rollback event subscribers are simply logging to console) \
  -> `user-counter-processor` increases counter (+1)\
  -> `user-verification-processor` verifies user (delays 10s) then publish back an event `UserVerifiedEvent`\
  -> `saga/create-user` listens on the event `UserVerifiedEvent`, set user `Active=true` and end the flow

### User Deletion Flow (Hard-Delete, we can easily change to Soft-Delete)
  -> Account service dispatch a command to remove user from DB
  -> `saga/delete-user` acts as a saga orchestrator to handle `User Deletion flow`, handle business and rollback if something went wrong (currently rollback event subscribers are simply logging to console) \
  -> `user-counter-processor` decreases counter (-1)\

## Technologies stack
- [Golang](https://golang.org/) programming language
- [Mux](https://github.com/gorilla/mux) building routes
- [CockroachDB](https://www.cockroachlabs.com/) database
- [gorm](https://gorm.io/) ORM library
- [NATS](https://nats.io/) messaging system
- [NATS Streaming Server](https://github.com/nats-io/nats-streaming-server) lightweight streaming platform build on NATS
- [CQRS & Event Sourcing](https://threedots.tech/post/basic-cqrs-in-go/)
- [Orchestrator Saga](https://www.prakharsrivastav.com/posts/saga-orchestration-in-microservices/)
- [Swagger](https://github.com/go-swagger/go-swagger) TODO
- [Validator](https://github.com/go-playground/validator) TODO
- [Docker](https://docs.docker.com/compose/) TODO

## Prerequisites
- Run NATS Streaming Server with Docker [here](https://hub.docker.com/_/nats-streaming)
- Install CockroachDB [here](https://www.cockroachlabs.com/docs/stable/install-cockroachdb-mac.html)

## How to start
### Create database & user
```
CREATE DATABASE IF NOT EXISTS xxx;
CREATE USER xxx WITH PASSWORD password;
GRANT ALL ON DATABASE xxx TO xxx;
```
### Install libraries via go install command
```
cd account/cmd
go install
```

### Start account service
```
go start account/cmd/main.go 
```

#### GET http://localhost:17601/users/report -> Get users count
#### GET http://localhost:17601/users?page=1&limit=20&search=something -> Get users
#### POST http://localhost:17601/users -> Create new user
#### GET http://localhost:17601/users/{uuid} -> Get user by id
#### PUT http://localhost:17601/users/{uuid} -> Update a user
#### DELETE http://localhost:17601/users/{uuid} -> Delete a user


### Start user counter processor
```
go run user-counter-processor/main.go
```

### Start user verification processor
```
go run user-counter-processor/main.go
```